# Experiment Suite - usage demo #

**Experiment Suite** (or *es*) is a project aimed to simplify setting up different experiments using any stimuli configuration. Typical *es* experiment consists of presenting any stimuli to subject and recording any data in parallel. During the experiment subject performs any actions corresponding to the current presented stimulus, while on the recording part we take in account both current stimulus info (which stimulus is being demonstrated right now - so called *stimulus event*) and all incoming data, which will be associated with the stimulus onset/unset and corresponding user action. Both stimuli info and target data are stored in the main log file. So, in the final, we are able to analyze the main log in terms which event-related (stimuli-realted) data patterns are distinguished/repeatable/your_criteria or not.

To set up experiment using *es* your need to create a python script and place all workflow configuration into it. Generally, you need to initialize, configure and interconnect 4 main components:

 - module for stimuli presentation or *presenter*.    
   Currently available presenters:
    - WebPresenter - presentation of images using web interface (https://bitbucket.org/barleyjuice/es_presentation_web/);

 - module for recording data from external device or *recorder*.    
   Currently available recorders:
    - UnicornRecorder - Windows-compatible module for EEG-signal recording from Unicorn Hybrid Black BCI (https://bitbucket.org/barleyjuice/es_recording_unicorn/);

 - module for manage stimuli demonstration sequence and timings or *experiment scheme*, or *controller*.    
   Currently available controllers:
    - P300Experiment - a module for handling experiment with stimuli based on images presenter by odd-ball paradigm. A part of core *es* package (https://bitbucket.org/barleyjuice/experiment-suite/);
    - SSVEPExperiment - a module for handling experiment with stimuli based on flickering images presenter. **Note: Currentry under development, there is no ready-to-use presenter for it.** A part of core *es* package (https://bitbucket.org/barleyjuice/experiment-suite/);

 - module to connect and launch them all or *application*.    
   Currently available applications:
    - Application - an engine for launching the components within multiple threads. A part of core *es* package (https://bitbucket.org/barleyjuice/experiment-suite/).

To start setting up an experiment you should choose & pick modules you prefer and configure them within Python code. Also you should provide description of your stimuli set: stimuli categories, stimuli source (for images) or frequency (for flickers), how many times each image can be repeated, etc. In more detail these steps are described in the attached [example](example.py), and for each component there is detailed documentation in the corresponding repo.

As for the running the experiment, after the configuration of all abovementioned modules, you need to launch the *application* instance using `app.run()` function. In the [example script](example.py) it is placed inside main section, which means it will be called on the execution of the script via `python3 example.py`:

```python
if __name__ == '__main__':
    # run the app
    app.run()
```

So, after the launching the application in any way (e.g. as shown above) the all processes will be started and also the application will start printing some logs (the example script uses WebPresenter, so there are some logs of the webserver):

```
['anger', 'happiness', 'sadness']
2
7
11
Application is running!
Attempting to run flask app
WebSocket transport not available. Install simple-websocket for improved performance.
 * Serving Flask app 'experiments_app' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:8081/ (Press CTRL+C to quit)
```

This mean that all is sucessfully launched. If you face any issues, check if you have installed all necessary python modules into the current environment, check the modules' configuration in your code and also ensure that the chosen port is not busy.

Next, open a new tab in a web-browser (current version of WebPresenter is tested only for Google Chrome, but you can try any browser) and go to the url http://localhost:8081/ (or your specific port number). You will see a web page with the promt on the left to enter the subject id. You can enter subject id, which will be inserted in a log name. After that, if you press "Start session" button, the process of stimuli shown will be started. Stimuli will be sequentially presented until the stimuli queue will be empty. To interrupt the process ahead of schedule, press Ctrl+X. In both cases, if the process is completed or interrupted, there will be a brief summary file created in the same directory where the application process was launched, and a log file (if the recorder was used). **Important note: currently recorder is not handling properly the resetting of the session or reusing it for another subject id. These cases will be fixed later, and now to launch experiment session again you should restart the application process and reload the page with ui in the browser (F5).**

