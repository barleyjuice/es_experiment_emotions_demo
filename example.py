# import workflow control module - P300, and the data structure for describing stimuli
from es_core.experiments import P300Experiment, ImageDesc

# import recoring module
# uncomment following line if you have the Unicorn BCI and want to test with it
# from es_recording.unicorn import UnicornRecorder

# import presentation module
from es_presentation.web import WebPresenter

# import application module
from es_core import Application

import os

# initialize stimuli set: filename, category and repetitions
# as a result, during the experiment will be shown 18 images 
# (sum of the repetitions) of 3 categories - anger, happiness, sadness. 
# Each category will have 6 images - so, the categories are balanced in amount. 
# If you want to break the balance for some reason, increase or decrease 
# repetitions for particular stimuli
stimuli = [
    ImageDesc("anger1.jpg", "anger", 3), 
    ImageDesc("anger2.jpg", "anger", 3),
    ImageDesc("happiness1.jpg", "happiness", 3),
    ImageDesc("sadness.jpg", "sadness", 6),
    ImageDesc("happiness2.jpg", "happiness", 3)
]

# search all abovementioned images in the folder ./images/emotions
stimuli_folder = os.path.join(os.getcwd(), 'images', 'emotions')

# create a presenter - WebPresenter on the port 8081 (you may choose another port)
presenter = WebPresenter(port=8081, stimuli_folder=stimuli_folder)

# create an experiment workflow scheme with already created stimuli
experimentScheme = P300Experiment(images=stimuli)

# create the recorder module with extra "EVENT_MARKER" column in log for stimuli info
# uncomment following lines if you have the Unicorn BCI and want to test with it
# recorder = UnicornRecorder(marker_channel="EVENT_MARKER")
# app = Application(presenter=presenter, experimentScheme=experimentScheme, recorder=recorder)

# create an app
# comment following line if you have the Unicorn BCI and want to test with it
app = Application(presenter=presenter, experimentScheme=experimentScheme)

if __name__ == '__main__':
    # run the app
    app.run()

